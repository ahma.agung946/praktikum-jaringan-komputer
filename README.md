# Praktikum Jaringan Komputer

// Nama : Muhammad Ulwan Shidqi Athallah
// NRP  : 4210181016

// Description  :
/*
this program acted as UDP client
1-client declare its IP and Port
2-in client program, it ask the client username, and print the name everytime 
    the server send spread the data
3-the program shows "server_name nyambung" when the ip is binded
4-using for, console syntax receive input as a string. then send it as 
    ASCII bytes data
5-then the program shows the message that received from server
6-after limited number of sending and receiving. the udpclient closed.

*/